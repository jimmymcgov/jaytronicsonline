angular.module('JayTronicsOnline', [
    'ui.router',
    'ngCookies',
    'JayTronicsOnline.IteratorService',
    'JayTronicsOnline.navbar',
    'JayTronicsOnline.starRating',
    'JayTronicsOnline.UpdateStockController',
    'JayTronicsOnline.HomepageController',
    'JayTronicsOnline.ItemsPageController',
    'JayTronicsOnline.SearchItemsPageController',
    'JayTronicsOnline.CustomersController'
]).
config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/homepage');


    $stateProvider
        .state('homepage',{
            url:'/homepage',
            templateUrl:'resources/js/views/homepage.html',
            controller:'HomepageController'
        })
        .state('update-stock',{
            url:'/update-stock',
            templateUrl:'resources/js/views/update-stock.html',
            controller:'UpdateStockController'
        })
        .state('items',{
            url:'/items/:var',
            templateUrl:'resources/js/views/items.html',
            controller:'ItemsPageController'
        })
        .state('search-items',{
            url:'/search-items/:var',
            templateUrl:'resources/js/views/search-items.html',
            controller:'SearchItemsPageController'
        })
        .state('customers',{
            url:'/customers',
            templateUrl:'resources/js/views/customers.html',
            controller:'CustomersController'
        });
});
