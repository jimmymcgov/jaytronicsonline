angular.module('JayTronicsOnline.UpdateStockController', []).
controller('UpdateStockController', function ($scope, $state, $http, $rootScope, $cookieStore, Stock) {

    $scope.stock = Stock;
    console.log($scope.stock, ' - stock');

    $scope.addStock = {};
    $scope.addStockSubmit = function () {
        console.log($scope.addStock, "Stock Details sent to Rest Controller");

        $http.post('http://localhost:8080/api/addStock', JSON.stringify($scope.addStock))
            .then(function (response) {
                if(response.status = 200){

                    console.log(response.data);
                    if(response.data != ""){
                        console.log("SUCCESS Jimmy, it worked");
                        // $state.go('login');
                    }

                }
            }).catch("Didn't work")
    };

    $scope.loadUpdateModal = function (item) {


        $rootScope.theCounter = item.stockLevel;
        $scope.updateRead = true;
        $scope.updateItem = {id: item.id,
                            title: item.title,
                            manufacturer: item.manufacturer,
                            price: item.price,
                            category: item.category, image: item.image,
                            stockLevel: item.stockLevel
        };

    };




    $scope.increment = function () {
        $rootScope.theCounter++;
    };

    $scope.decrement = function () {
        $rootScope.theCounter =  $rootScope.theCounter -1;
    };

    $scope.updateItemSubmit = function (item) {

        item.stockLevel = $rootScope.theCounter;
        $http.post('http://localhost:8080/api/addStock', JSON.stringify(item))
            .then(function (response) {
                if(response.status = 200){

                    console.log(response.data);
                    if(response.data != ""){
                        console.log("SUCCESS Jimmy, it worked");
                        // $state.go('login');
                    }

                }
            }).catch("Didn't work")
    };

    $scope.removeItem= function (item) {

        $http.post('http://localhost:8080/api/removeStock', item.id)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        console.log("SUCCESS Jimmy, it worked");
                        // $state.go('login');
                    }

                }
            }).catch("Didn't work");

        $('.removerow').on('click', function() {
            $(this).closest('tr').remove();
        });


    };

    // function deleteRow(r) {
    //     var i = r.parentNode.parentNode.rowIndex;
    //     document.getElementById("myTable").deleteRow(i);
    // }

}).
factory('Stock', function ($http) {


    var Stock = {};



    $http.get('http://localhost:8080/api/getAllStock')
        .then(function (response) {
            if(response.status = 200){

                if(response.data != ""){
                    Stock.all = response.data;
                }

            }
        }).catch("Didn't work");


    return Stock;
});