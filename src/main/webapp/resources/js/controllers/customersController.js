angular.module('JayTronicsOnline.CustomersController', []).
controller('CustomersController', function (IteratorService, $scope, $state, $http, $rootScope, $cookieStore, Stock) {


    getCustomers();

    function getCustomers() {
        $http.get('http://localhost:8080/api/getAllCustomers')
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        $scope.customerList = response.data;
                    }

                }
            }).catch("Didn't work");
    }


    $scope.goToHistory = function (customer) {

        $http.post('http://localhost:8080/api/getCartByCustomer', customer.id)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        $scope.cart = response.data;
                        console.log($scope.cart, ' - customers cart');
                        getPurchaseList($scope.cart);

                    }

                }
            }).catch("Didn't work");
    };




    function getPurchaseList(cartList) {

        $scope.unpaidCartList = [];
        $scope.purchaseList = [];

        var iterator = new IteratorService.Iterator(cartList);

        for(var cart= iterator.first(); iterator.hasNext(); cart = iterator.next()){
            if(cart.paid === false){
                $scope.unpaidCartList.push(cart);
                getTotalCartPrice($rootScope.unpaidCartList);
            }else {
                $scope.purchaseList.push(cart);
            }
        }

        console.log($scope.unpaidCartList, ' - unpaid cart');
        console.log($scope.purchaseList, ' - paid cart');
    }

    function getTotalCartPrice(cartList) {

        var iterator = new IteratorService.Iterator(cartList);

        $rootScope.totalCartPrice = 0;
        for(var cart= iterator.first(); iterator.hasNext(); cart = iterator.next()){
            $rootScope.totalCartPrice = $scope.totalCartPrice + cart.item.price;
            console.log($rootScope.totalCartPrice, ' - total price');
        }
    }

    $scope.counter = 1;

    $scope.increment = function () {
        $scope.counter++
    };

    $scope.decrement = function () {
        $scope.counter =  $scope.counter -1;
    };



});