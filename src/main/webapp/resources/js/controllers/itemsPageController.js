angular.module('JayTronicsOnline.ItemsPageController', []).
controller('ItemsPageController', function (IteratorService, $scope, $state, $http, $stateParams, $cookieStore, $rootScope) {

    console.log($stateParams.var, " :param im receiving");
    $scope.itemType = $stateParams.var;

    var customer = $cookieStore.get('cookie');

    if(customer){
        getCart();
    }

    $http.post('http://localhost:8080/api/getStockByCategory', $scope.itemType)
        .then(function (response) {
            if(response.status = 200){
                if(response.data != ""){
                    $scope.items = response.data;
                    console.log($scope.items, ' - list of items');
                }
            }
        }).catch("Didn't work");


    function getCart() {
        //show full shopping cart paid and unpaid!
        $http.post('http://localhost:8080/api/getCartByCustomer', customer.id)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        $scope.cart = response.data;
                        console.log($scope.cart, ' - customers cart');
                        getCartLists($scope.cart);

                    }

                }
            }).catch("Didn't work");


    }


    function getCartLists(cartList) {

        $rootScope.unpaidCartList = [];
        $rootScope.paidCartList = [];
        // var iterator = new Iterator(cart);
        var iterator = new IteratorService.Iterator(cartList);

        for(var cart= iterator.first(); iterator.hasNext(); cart = iterator.next()){
            if(cart.paid === false){
                $rootScope.unpaidCartList.push(cart);
                getTotalCartPrice($rootScope.unpaidCartList);
            }else {
                $rootScope.paidCartList.push(cart);
            }
        }

        console.log($rootScope.unpaidCartList, ' - unpaid cart');
        console.log($rootScope.paidCartList, ' - paid cart');
    }



    $scope.addToCart = function (item) {
        var sendToCart = {};
        sendToCart.customer = String(customer.id);
        sendToCart.item = String(item.id);

        if($scope.counter <= item.stockLevel){
            sendToCart.quantity = String($scope.counter);

            $http.post('http://localhost:8080/api/addToCart', sendToCart)
                .then(function (response) {
                    if(response.status = 200){

                        if(response.data != ""){
                            var cart = response.data;
                            $rootScope.unpaidCartList.push(cart);
                            getTotalCartPrice($rootScope.unpaidCartList);
                            console.log(' added to cart ');
                        }

                    }
                }).catch("Didn't work");
        }
        else {
            alert('stock level is ' + item.stockLevel)
        }

    };

    function getTotalCartPrice(cartList) {
        $rootScope.totalCartPrice = 0;

        var iterator = new IteratorService.Iterator(cartList);


        for(var cart = iterator.first(); iterator.hasNext(); cart = iterator.next()){
            $rootScope.totalCartPrice = $scope.totalCartPrice + cart.item.price;
            console.log($rootScope.totalCartPrice, ' - total price');
        }
    }



    $scope.addRating = function (rating, item) {
        alert('You have added a star rating of ' +rating+ ' to ' +item.title);

        var starRating = {};
        starRating.id = String(item.id);
        starRating.rating = String(rating);

        $http.post('http://localhost:8080/api/addRating', starRating)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        console.log('Rating Added!');
                    }

                }
            }).catch("Didn't work");
    };

    $scope.starRating = 0;

    $scope.options = [1,2,3,4,5,6,7,8,9,10];





    $scope.counter = 1;

    $scope.increment = function () {
        $scope.counter++
    };

    $scope.decrement = function () {
        $scope.counter =  $scope.counter -1;
    };



    $scope.goToItem = function (item) {
        $state.go('view-item', {
            var: item.id
        });
    };



    $scope.setItem = function (item) {
        $scope.theItem = item;
    };

    $scope.addReview = function (description) {


        var review = {};
        review.itemId = String($scope.theItem.id);
        review.review = String(description);
        review.customerId = String(customer.id);


        $http.post('http://localhost:8080/api/addReview', review)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        $scope.theItem.reviews = response.data;
                        console.log('Review Added!');
                    }

                }
            }).catch("Didn't work");
    }




}).filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);