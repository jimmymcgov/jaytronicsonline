angular.module('JayTronicsOnline.SearchItemsPageController', []).
controller('SearchItemsPageController', function (IteratorService, $scope, $state, $http, $rootScope, $cookieStore, Stock) {


    $scope.stock = Stock;
    console.log($scope.stock, ' - stock');
    $scope.orderByField = 'firstName';
    $scope.reverseSort = false;

    var customer = $cookieStore.get('cookie');

    if(customer){
    getCart()
    }


    $scope.loadUpdateModal = function (item) {

        $scope.updateRead = true;
        $scope.updateItem = {id: item.id,
            title: item.title,
            manufacturer: item.manufacturer,
            price: item.price,
            category: item.category, image: item.image};
    };


    function getCart() {
        //show full shopping cart paid and unpaid!
        $http.post('http://localhost:8080/api/getCartByCustomer', customer.id)
            .then(function (response) {
                if(response.status = 200){

                    if(response.data != ""){
                        $scope.cart = response.data;
                        console.log($scope.cart, ' - customers cart');
                        getCartLists($scope.cart);

                    }

                }
            }).catch("Didn't work");
    }



    function getCartLists(cartList) {

        $rootScope.unpaidCartList = [];
        $rootScope.paidCartList = [];

        var iterator = new IteratorService.Iterator(cartList);

        for(var cart= iterator.first(); iterator.hasNext(); cart = iterator.next()){
            if(cart.paid === false){
                $rootScope.unpaidCartList.push(cart);
                getTotalCartPrice($rootScope.unpaidCartList);
            }else {
                $rootScope.paidCartList.push(cart);
            }
        }

        console.log($rootScope.unpaidCartList, ' - unpaid cart');
        console.log($rootScope.paidCartList, ' - paid cart');
    }

    function getTotalCartPrice(cartList) {

        var iterator = new IteratorService.Iterator(cartList);

        $rootScope.totalCartPrice = 0;
        for(var cart= iterator.first(); iterator.hasNext(); cart = iterator.next()){
            $rootScope.totalCartPrice = $scope.totalCartPrice + cart.item.price;
            console.log($rootScope.totalCartPrice, ' - total price');
        }
    }

    $scope.counter = 1;

    $scope.increment = function () {
        $scope.counter++
    };

    $scope.decrement = function () {
        $scope.counter =  $scope.counter -1;
    };

    $scope.addToCart = function (item) {
        var sendToCart = {};
        sendToCart.customer = String(customer.id);
        sendToCart.item = String(item.id);
        if($scope.counter <= item.stockLevel) {
            sendToCart.quantity = String($scope.counter);
            $http.post('http://localhost:8080/api/addToCart', sendToCart)
                .then(function (response) {
                    if (response.status = 200) {

                        if (response.data != "") {
                            var cart = response.data;
                            $rootScope.unpaidCartList.push(cart);
                            getTotalCartPrice($rootScope.unpaidCartList);
                            console.log(' added to cart ');
                        }

                    }
                }).catch("Didn't work");
        }else {
            alert('stock level is ' + item.stockLevel)
        }
    };


    // function deleteRow(r) {
    //     var i = r.parentNode.parentNode.rowIndex;
    //     document.getElementById("myTable").deleteRow(i);
    // }

}).
factory('Stock', function ($http) {


    var Stock = {};



    $http.get('http://localhost:8080/api/getAllStock')
        .then(function (response) {
            if(response.status = 200){

                if(response.data != ""){
                    Stock.all = response.data;
                }

            }
        }).catch("Didn't work");


    return Stock;
});