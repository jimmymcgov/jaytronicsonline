angular.module('JayTronicsOnline.navbar',[]).
directive('navbar', function($cookieStore, $http){
    return{
        restrict:'AE',
        scope : {
        },
        controller: function ($scope, $state, $stateParams, $rootScope) {

            getTheCookie();

            console.log(' you are in navbar controller');

            function getTheCookie() {
                var user = $cookieStore.get('cookie');

                if(!user){
                    $rootScope.customerAccount = false;
                    $rootScope.loggedOutAccount = true;
                    $rootScope.adminAccount = false;
                }else if(user.accountType === "Administrator"){
                    $rootScope.customerAccount = false;
                    $rootScope.loggedOutAccount = false;
                    $rootScope.adminAccount = true;
                }else if(user.accountType === "Customer"){
                    $rootScope.customerAccount = true;
                    $rootScope.loggedOutAccount = false;
                    $rootScope.adminAccount = false;
                }
            }



            $scope.logOut = function () {
                $cookieStore.remove('cookie');
                $rootScope.customerAccount = false;
                $rootScope.loggedOutAccount = true;
                $rootScope.adminAccount = false;

            };


            $scope.goToHome = function () {
                $state.go('homepage');
            };

            $scope.goToUpdateItems = function () {
                $state.go('update-stock');
            };

            $scope.goToSearchItems = function () {
                $state.go('search-items');
            };

            $scope.goToCustomers = function () {
                $state.go('customers');
            };

            $scope.removeFromCart = function (cart) {


                $http.post('http://localhost:8080/api/removeFromCart', cart.id)
                    .then(function (response) {
                        if(response.status = 200){

                            if(response.data != ""){
                                console.log(response.data);
                            }

                        }
                    }).catch("Didn't work");


                var index = $rootScope.unpaidCartList.indexOf(cart);
                $rootScope.unpaidCartList.splice(index, 1);
            };


            $scope.buyCart = function () {
                console.log(' in buy cart ');
                for(var i=0; i<$rootScope.unpaidCartList.length; i++){
                    var cart = $rootScope.unpaidCartList[i];

                    var cartAndDiscount = {};
                    cartAndDiscount.id = String(cart.id);
                    cartAndDiscount.discountCode = String($scope.discountCode);
                    $http.post('http://localhost:8080/api/buyCartItem', cartAndDiscount)
                        .then(function (response) {
                            if(response.status = 200){

                                if(response.data != ""){
                                    console.log("SUCCESS")
                                }

                            }
                        }).catch("Didn't work");

                }

            };






            // login functionality start


            $scope.paymentMethodOptions = [{
                name: 'VISA',
                checked: false
            }, {
                name: 'MASTERCARD',
                checked: false
            }, {
                name: 'PAYPAL',
                checked: false
            }
            ];

            $scope.updateSelection = function(position, paymentMethodOptions) {
                angular.forEach(paymentMethodOptions, function(subscription, index) {

                    if (position != index){
                        subscription.checked = false;
                    }else if(position === index){
                        console.log(subscription.name);
                        $scope.regCustomer.paymentMethod = subscription.name;
                    }

                });
            };

            $scope.regCustomer = {};

            $scope.regCustomerSubmit = function () {
                console.log($scope.regCustomer, "Admin Details sent to Rest Method");

                $scope.regCustomer.accountType = "Customer";
                $http.post('http://localhost:8080/api/regCustomer', JSON.stringify($scope.regCustomer))
                    .then(function (response) {
                        if(response.status = 200){

                            console.log(response.data);
                            if(response.data != ""){
                                console.log("SUCCESS Jimmy, it worked");
                                // $state.go('login');
                            }

                        }
                    }).catch("Didn't work");

            };



            $scope.loginSubmit = function () {

                console.log($scope.loginType, " - login type");
                //if option = something
                if($scope.loginType === "Administrator"){
                    loginAdmin();
                }
                if($scope.loginType === "Customer"){
                    loginCustomer();
                }

            };

            function loginAdmin() {

                //admin arraylists
                $http.post("http://localhost:8080/api/loginAdmin", $scope.loginUser)
                    .then(function (response) {
                        if (response.status = 200) {

                            if (response.data != "") {
                                console.log("SUCCESSSSSSSSS");
                                $scope.loginUserResult = response.data;
                                console.log($scope.loginUserResult, ' - if it worked');
                                addCookie($scope.loginUserResult);
                                $rootScope.customerAccount = false;
                                $rootScope.loggedOutAccount = false;
                                $rootScope.adminAccount = true;
                                $state.go('homepage');
                                // findTheUser($scope.loginUserList);
                            }else {
                                alert('Email or password is incorrect!!');
                            }


                        }
                    }).catch("Didnt work");


            }

            function loginCustomer() {

                //admin arraylists
                $http.post("http://localhost:8080/api/loginCustomer", $scope.loginUser)
                    .then(function (response) {
                        if (response.status = 200) {

                            if (response.data != "") {
                                console.log("SUCCESSSSSSSSS");
                                $scope.loginUserResult = response.data;
                                console.log($scope.loginUserResult, ' - if it worked');
                                addCookie($scope.loginUserResult);
                                $rootScope.customerAccount = true;
                                $rootScope.loggedOutAccount = false;
                                $rootScope.adminAccount = false;
                                $state.go('homepage');


                                // findTheUser($scope.loginUserList);
                            }else {
                                alert('Email or password is incorrect!!');
                            }


                        }
                    }).catch("Didnt work");


            }

            function addCookie(user) {
                $cookieStore.put('cookie', user);
            }


            // login functionality end


            $scope.goToItemPage = function(product){
                $state.go('items', {
                    var: product
                });
            };








        },
        templateUrl:'resources/js/directives/NavbarDirective/navbar.html',
        replace:true
    }


}).filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);