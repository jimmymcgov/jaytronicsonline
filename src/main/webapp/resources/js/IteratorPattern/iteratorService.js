angular.module('JayTronicsOnline.IteratorService', [])
    .service('IteratorService', function () {

            //Iterator Pattern
            this.Iterator = function (items) {
                this.index = 0;
                this.items = items;
            };

            this.Iterator.prototype = {
                first: function () {
                    this.reset();
                    return this.next();
                },
                next: function () {
                    return this.items[this.index++];
                },
                hasNext: function () {
                    return this.index <= this.items.length;
                },
                reset: function () {
                    this.index = 0;
                }
            };

    });