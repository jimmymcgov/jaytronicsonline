package com.example.observer;

import com.example.entity.Review;
import com.example.entity.Stock;

import java.util.ArrayList;
import java.util.List;


public class Subject {


    private List<Observer> observers = new ArrayList<Observer>();
    private Stock stock;


    public Stock getState() {
        return stock;

    }


    public void setReviews(Review review) {
        this.stock = stock;
        notifyAllObservers();

    }


    public void attach(Observer observer) {
        observers.add(observer);

    }


    private void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.update();

        }

    }


}
