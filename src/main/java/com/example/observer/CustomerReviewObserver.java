package com.example.observer;

import com.example.entity.Administrator;
import com.example.entity.Customer;
import com.example.entity.Review;
import com.example.entity.Stock;
import com.example.service.impl.AdministratorService;
import com.example.service.impl.CustomerService;
import com.example.service.impl.StockService;

import java.util.List;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class CustomerReviewObserver extends Observer{


    public CustomerReviewObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {

        StockService stockService = new StockService();
        List<Stock> stockList = stockService.findAll();


        CustomerService customerService = new CustomerService();
        List<Customer> customerList = customerService.findAll();


        for (Stock stock : stockList){
            List<Review> reviewList = stock.getReviews();

            for (Review review : reviewList){
                for (Customer customer : customerList){
                    if(review.getCustomer().getId() == customer.getId()){


                        //send an email to the customer that a new review has been added for a stock they reviewed
                        Properties mailServerProperties;
                        Session getMailSession;
                        MimeMessage generateMailMessage;

                        InetAddress ip;
                        String hostname = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostAddress();
                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        mailServerProperties = System.getProperties();
                        mailServerProperties.put("mail.smtp.port", "587");
                        mailServerProperties.put("mail.smtp.auth", "true");
                        mailServerProperties.put("mail.smtp.starttls.enable", "true");


                        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
                        generateMailMessage = new MimeMessage(getMailSession);
                        try {
                            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(customer.getEmail()));
                            generateMailMessage.setSubject("New Review On An Item You Reviewed!");
                            String emailBody = "A new Review has been added to " + subject.getState().getTitle() +
                                    " and now there are " + subject.getState().getReviews().size() + " reviews including yours.";
                            generateMailMessage.setContent(emailBody, "text/html");


                            Transport transport = getMailSession.getTransport("smtp");

                            transport.connect("smtp.gmail.com", "c13707355@mydit.ie", "(NOJ)!874!mp");
                            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
                            transport.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }


        }






    }
}
