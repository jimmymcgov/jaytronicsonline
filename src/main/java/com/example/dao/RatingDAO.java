package com.example.dao;

import com.example.entity.Customer;
import com.example.entity.Rating;
import com.example.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingDAO extends JpaRepository<Rating, Integer>{

    List<Rating> findAll();
//    List<Rating> findByStock(Stock stock);
//    List<Rating> findByCustomer(Customer customer);

}
