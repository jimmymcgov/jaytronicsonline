package com.example.dao;

import com.example.entity.Customer;
import com.example.entity.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingCartDAO extends JpaRepository<ShoppingCart, Integer>{

    List<ShoppingCart> findAll();
    List<ShoppingCart> findByCustomer(Customer customer);
    ShoppingCart findById(int id);
}
