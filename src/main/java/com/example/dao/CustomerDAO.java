package com.example.dao;

import com.example.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDAO extends JpaRepository<Customer, Integer>{

    List<Customer> findAll();
    Customer findById(int id);
    Customer findByEmail(String email);

}
