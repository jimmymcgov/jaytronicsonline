package com.example.dao;

import com.example.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockDAO extends JpaRepository<Stock, Integer>{

    List<Stock> findAll();
    void delete(Stock stock);
    Stock findById(int id);
    List<Stock> findByCategory(String category);
    List<Stock> findByTitle(String title);
    List<Stock> findByManufacturer(String manufacturer);


}
