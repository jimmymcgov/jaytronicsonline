package com.example.dao;

import com.example.entity.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdministratorDAO extends JpaRepository<Administrator, Integer>{

    List<Administrator> findAll();
    Administrator findById(int id);
    Administrator findByEmail(String email);

}
