package com.example.dao;

import com.example.entity.Customer;
import com.example.entity.Review;
import com.example.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewDAO extends JpaRepository<Review, Integer>{

    List<Review> findAll();

}
