package com.example.strategy;


public class DiscountCodeStrategy implements IDiscountCodeStrategy {
    @Override
    public double getDiscount(String code, double price) {

        switch (code) {
            case "ILOVEDIT":
                return price * .75;
            case "OAPTRONICS":
                return price * .85;
            case "TECHGEEK2000":
                return price * .90;
            default:
                return price;
        }
    }
}
