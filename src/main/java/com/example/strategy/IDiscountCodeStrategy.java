package com.example.strategy;

public interface IDiscountCodeStrategy {

    double getDiscount(String code, double price);
}
