package com.example.controllers;

import com.example.entity.Customer;
import com.example.entity.ShoppingCart;
import com.example.entity.Stock;
import com.example.service.ICustomerService;
import com.example.service.IShoppingCartService;
import com.example.service.IStockService;
import com.example.strategy.DiscountCodeStrategy;
import com.example.strategy.IDiscountCodeStrategy;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import static java.lang.Math.toIntExact;

import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class ShoppingCartController {

    private IShoppingCartService iShoppingCartService;
    private IStockService iStockService;
    private ICustomerService iCustomerService;
    private IDiscountCodeStrategy iDiscountCodeStrategy;

    @Autowired
    public void setiShoppingCartService(IShoppingCartService iShoppingCartService) {
        this.iShoppingCartService = iShoppingCartService;
    }
    @Autowired
    public void setiStockService(IStockService iStockService) {
        this.iStockService = iStockService;
    }

    @Autowired
    public void setiCustomerService(ICustomerService iCustomerService) {
        this.iCustomerService = iCustomerService;
    }


    @RequestMapping(value = "/addToCart", method = RequestMethod.POST, produces = "application/json")
    public ShoppingCart addStock(@RequestBody String customerAndItem) throws ParseException {


        JSONObject json = (JSONObject)new JSONParser().parse(customerAndItem);
        System.out.println(json);
        String customerId = (String) json.get("customer");
        String itemId = (String) json.get("item");
        String quantityString = (String) json.get("quantity");
        Date date = new Date();

        Customer customer = iCustomerService.findCustomerById(Integer.parseInt(customerId));
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setCustomer(customer);
        shoppingCart.setPaid(false);

        Stock stock = iStockService.findStockById(Integer.parseInt(itemId));
        stock.setQuantity(Integer.parseInt(quantityString));
        stock.setPrice(stock.getPrice()*stock.getQuantity());
        shoppingCart.setItem(stock);
        shoppingCart.setDate(date);

        iShoppingCartService.addAShoppingCart(shoppingCart);

        return shoppingCart;


    }


    @RequestMapping(value = "/removeFromCart", method = RequestMethod.POST, produces = "application/json")
    public void removeFromCart(@RequestBody String cartId) throws ParseException {


        ShoppingCart shoppingCart = iShoppingCartService.findShoppingCartById(Integer.parseInt(cartId));
        iShoppingCartService.deleteShoppingCart(shoppingCart);
    }

    @RequestMapping(value = "/buyCartItem", method = RequestMethod.POST, produces = "application/json")
    public void buyCartItem(@RequestBody String cartAndDiscount) throws ParseException {

        JSONObject json = (JSONObject)new JSONParser().parse(cartAndDiscount);
        String cartId = (String) json.get("id");
        String discountCode = (String) json.get("discountCode");

        ShoppingCart theCart = iShoppingCartService.findShoppingCartById(Integer.parseInt(cartId));

        Stock stock = iStockService.findStockById(theCart.getItem().getId());
        int stockLevel = stock.getStockLevel() - 1;

        DiscountCodeStrategy discountCodeStrategy = new DiscountCodeStrategy();
        double price = discountCodeStrategy.getDiscount(discountCode, stock.getPrice());

        double cartPrice = price * stock.getQuantity();
        theCart.setPrice(cartPrice);
        stock.setStockLevel(stockLevel);

        theCart.setItem(stock);
        theCart.setPaid(true);
        iShoppingCartService.addAShoppingCart(theCart);
    }

    @RequestMapping(value = "/getCartByCustomer", method = RequestMethod.POST, produces = "application/json")
    public List<ShoppingCart> getCartByCustomer(@RequestBody String customerId){

        Customer customer = iCustomerService.findCustomerById(Integer.parseInt(customerId));

        return iShoppingCartService.findShoppingCartByCustomer(customer);
    }

}