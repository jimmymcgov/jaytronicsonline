package com.example.controllers;

import com.example.entity.Administrator;
import com.example.service.IAdministratorService;
import com.example.service.impl.AdministratorService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AdministratorController {

//    private IAdministratorService iAdministratorService;



   AdministratorService administratorService = AdministratorService.getInstance();


    @RequestMapping(value = "/loginAdmin", method = RequestMethod.POST, produces = "application/json")
    public Administrator addAdmin(@RequestBody String adminDetails) throws ParseException {

        JSONObject json = (JSONObject)new JSONParser().parse(adminDetails);
        System.out.println(json);
        String email = (String) json.get("email");
        String password = (String) json.get("password");
        Administrator administrator = AdministratorService.getInstance().findAdminByEmail(email);
        if (administrator != null){
            //check password
            if(administrator.getPassword().equals(password)){
                return administrator;
            }else {
                return null;
            }

        }else{
            return null;
        }

    }

    @RequestMapping(value = "/getAllAdmin", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Administrator> getAllAdmin(){

        return AdministratorService.getInstance().findAll();
    }




}
