package com.example.controllers;

import com.example.entity.Customer;
import com.example.entity.ShoppingCart;
import com.example.entity.Stock;
import com.example.service.ICustomerService;
import com.example.service.IShoppingCartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerController {

    private ICustomerService iCustomerService;
    private IShoppingCartService iShoppingCartService;

    @Autowired
    public void setiCustomerService(ICustomerService iCustomerService) {
        this.iCustomerService = iCustomerService;
    }

    @Autowired
    public void setiShoppingCartService(IShoppingCartService iShoppingCartService) {
        this.iShoppingCartService = iShoppingCartService;
    }


    @RequestMapping(value = "/regCustomer", method = RequestMethod.POST, produces = "application/json")
    public void regCustomer(@RequestBody String customerDetails){
        Customer customer = new Customer();
        try {
            customer = new ObjectMapper().readValue(customerDetails, Customer.class);
            System.out.println(customer.toString());
            iCustomerService.addACustomer(customer);

        } catch (Exception e) {
            e.printStackTrace();
        }
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setCustomer(customer);

        iShoppingCartService.addAShoppingCart(shoppingCart);


    }

    @RequestMapping(value = "/loginCustomer", method = RequestMethod.POST, produces = "application/json")
    public Customer loginCustomer(@RequestBody String customerDetails) throws ParseException {

        JSONObject json = (JSONObject)new JSONParser().parse(customerDetails);
        System.out.println(json);
        String email = (String) json.get("email");
        String password = (String) json.get("password");
        Customer customer = iCustomerService.findCustomerByEmail(email);
        if (customer != null){
            if(customer.getPassword().equals(password)){
                return customer;
            }else {
                return null;
            }
        }else{
            return null;
        }

    }


    @RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Customer> getAllCustomers(){

        return iCustomerService.findAll();
    }

    @RequestMapping(value = "/getCustomerById", method = RequestMethod.POST, produces = "application/json")
    public Customer getCustomerById(@RequestBody String customerId){
        return iCustomerService.findCustomerById(Integer.parseInt(customerId));
    }

}