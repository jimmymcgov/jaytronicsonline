package com.example.controllers;

import com.example.entity.Customer;
import com.example.entity.Rating;
import com.example.entity.Review;
import com.example.entity.Stock;
import com.example.service.ICustomerService;
import com.example.service.IRatingService;
import com.example.service.IReviewService;
import com.example.service.IStockService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class StockController {

    private IStockService iStockService;
    private IRatingService iRatingService;
    private ICustomerService iCustomerService;
    private IReviewService iReviewService;

    @Autowired
    public void setiStockService(IStockService iStockService) {
        this.iStockService = iStockService;
    }
    @Autowired
    public void setiRatingService(IRatingService iRatingService) {
        this.iRatingService = iRatingService;
    }
    @Autowired
    public void setiCustomerService(ICustomerService iCustomerService){this.iCustomerService = iCustomerService;}
    @Autowired
    public void setiReviewService(IReviewService iReviewService){this.iReviewService = iReviewService;}


    @RequestMapping(value = "/addStock", method = RequestMethod.POST, produces = "application/json")
    public void addStock(@RequestBody String stockDetails){
        Stock stock = new Stock();
        try {

            stock = new ObjectMapper().readValue(stockDetails, Stock.class);
            System.out.println(stock.toString());
            iStockService.addStock(stock);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/addRating", method = RequestMethod.POST, produces = "application/json")
    public void addRating(@RequestBody String ratingDetails) throws ParseException {


        JSONObject jsonRating = (JSONObject)new JSONParser().parse(ratingDetails);
        System.out.println(jsonRating);
        String stockId = (String) jsonRating.get("id");
        String rating = (String) jsonRating.get("rating");
        Rating theRating = new Rating();
        theRating.setRating(Integer.parseInt(rating));
        iRatingService.addARating(theRating);

        Stock stock = iStockService.findStockById(Integer.parseInt(stockId));



        List<Rating> ratings = stock.getRatings();
        ratings.add(theRating);
        int totalRating = 0;
        double ratingAverage = 0;



        for(Rating rate : ratings){
            totalRating = totalRating + rate.getRating();
        }
        ratingAverage = totalRating / ratings.size();
        stock.setOverallRating(ratingAverage);
        stock.setRatings(ratings);

        iStockService.addStock(stock);
    }

    @RequestMapping(value = "/addReview", method = RequestMethod.POST, produces = "application/json")
    public List<Review> addReview(@RequestBody String reviewDetails) throws ParseException {


        JSONObject jsonReview = (JSONObject)new JSONParser().parse(reviewDetails);
        System.out.println(jsonReview);
        String stockId = (String) jsonReview.get("itemId");
        String customerId = (String) jsonReview.get("customerId");

        String reviewText = (String) jsonReview.get("review");
        Date date = new Date();
        Customer customer = iCustomerService.findCustomerById(Integer.parseInt(customerId));
        Stock stock = iStockService.findStockById(Integer.parseInt(stockId));

        Review review = new Review();
        review.setDate(date);
        review.setCustomer(customer);
        review.setReview(reviewText);

        List<Review> reviews = stock.getReviews();
        reviews.add(review);

        stock.setReviews(reviews);
        iReviewService.addAReview(review);
        iStockService.addStock(stock);

        return reviews;

    }

    @RequestMapping(value = "/removeStock", method = RequestMethod.POST, produces = "application/json")
    public void removeStock(@RequestBody String stockId){
       Stock stock = iStockService.findStockById(Integer.parseInt(stockId));

        iStockService.deleteStock(stock);

    }


    @RequestMapping(value = "/getAllStock", method = RequestMethod.GET, produces = "application/json")
    public List<Stock> getAllStock(){
        return iStockService.findAll();
    }

    @RequestMapping(value = "/getStockById", method = RequestMethod.POST, produces = "application/json")
    public Stock getStockById(@RequestBody String stockId){
        return iStockService.findStockById(Integer.parseInt(stockId));
    }

    @RequestMapping(value = "/getStockByCategory", method = RequestMethod.POST, produces = "application/json")
    public List<Stock> getStockByCategory(@RequestBody String category){
        return iStockService.findStockByCategory(category);
    }

    @RequestMapping(value = "/getStockByTitle", method = RequestMethod.GET, produces = "application/json")
    public List<Stock> getStockByTitle(@RequestBody String title){
        return iStockService.findStockByTitle(title);
    }

    @RequestMapping(value = "/getStockByManufacturer", method = RequestMethod.GET, produces = "application/json")
    public List<Stock> getStockByManufacturer(@RequestBody String manufacturer){
        return iStockService.findStockByManufacturer(manufacturer);
    }
}
