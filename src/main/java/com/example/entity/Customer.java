package com.example.entity;

import javax.persistence.*;

@Entity
public class Customer extends User{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String shippingAddress;
    private String paymentMethod;
    private String firstname;
    private String surname;
    private String image;

    public Customer(){}

    public Customer(String email, String password, String accountType) {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
