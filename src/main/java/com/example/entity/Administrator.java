package com.example.entity;

import javax.persistence.*;
@Entity
public class Administrator extends User{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    public Administrator(){}

    public Administrator(String email, String password) {
        super();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
