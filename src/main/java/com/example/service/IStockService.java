package com.example.service;


import com.example.entity.Stock;

import java.util.List;

public interface IStockService {

    List<Stock> findAll();
    void deleteStock(Stock stock);
    void addStock(Stock stock);
    Stock findStockById(int id);
    List<Stock> findStockByCategory(String category);
    List<Stock> findStockByTitle(String title);
    List<Stock> findStockByManufacturer(String manufacturer);
}
