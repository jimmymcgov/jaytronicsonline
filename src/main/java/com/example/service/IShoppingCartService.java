package com.example.service;

import com.example.entity.Customer;
import com.example.entity.ShoppingCart;

import java.util.List;

public interface IShoppingCartService {

    List<ShoppingCart> findAll();
    void addAShoppingCart(ShoppingCart shoppingCart);
    List<ShoppingCart> findShoppingCartByCustomer(Customer customer);
    ShoppingCart findShoppingCartById(int id);
    void deleteShoppingCart(ShoppingCart shoppingCart);
}
