package com.example.service;

import com.example.entity.Customer;

import java.util.List;

public interface ICustomerService {

    List<Customer> findAll();
    void addACustomer(Customer customer);
    Customer findCustomerById(int id);
    Customer findCustomerByEmail(String email);
}
