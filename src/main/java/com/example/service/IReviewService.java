package com.example.service;

import com.example.entity.Customer;
import com.example.entity.Review;
import com.example.entity.Stock;

import java.util.List;

public interface IReviewService {

    List<Review> findAll();
    void addAReview(Review review);
}
