package com.example.service.impl;

import com.example.dao.ShoppingCartDAO;
import com.example.entity.Customer;
import com.example.entity.ShoppingCart;
import com.example.service.IShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShoppingCartService implements IShoppingCartService{

    private ShoppingCartDAO shoppingCartDAO;

    @Autowired
    public void setShoppingCartDAO(ShoppingCartDAO s){this.shoppingCartDAO = s;}

    @Override
    public List<ShoppingCart> findAll() {
       return shoppingCartDAO.findAll();
    }

    @Override
    public void addAShoppingCart(ShoppingCart shoppingCart) {
        shoppingCartDAO.save(shoppingCart);
    }

    @Override
    public List<ShoppingCart> findShoppingCartByCustomer(Customer customer) {
        return shoppingCartDAO.findByCustomer(customer);
    }

    @Override
    public ShoppingCart findShoppingCartById(int id) {
        return shoppingCartDAO.findById(id);
    }

    @Override
    public void deleteShoppingCart(ShoppingCart shoppingCart) {
        shoppingCartDAO.delete(shoppingCart);
    }
}
