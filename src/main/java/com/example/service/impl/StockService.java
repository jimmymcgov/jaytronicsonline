package com.example.service.impl;

import com.example.dao.StockDAO;
import com.example.entity.Stock;
import com.example.service.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StockService implements IStockService{

    private StockDAO stockDAO;

    @Autowired
    public void setStockDAO(StockDAO s){this.stockDAO = s;}

    @Override
    public List<Stock> findAll() {
       return stockDAO.findAll();
    }

    @Override
    public void deleteStock(Stock stock) {
        stockDAO.delete(stock);
    }

    @Override
    public void addStock(Stock stock) {
            stockDAO.save(stock);
    }

    @Override
    public Stock findStockById(int id) {
        return stockDAO.findById(id);
    }

    @Override
    public List<Stock> findStockByCategory(String category) {
        return stockDAO.findByCategory(category);
    }

    @Override
    public List<Stock> findStockByTitle(String title) {
        return stockDAO.findByTitle(title);
    }

    @Override
    public List<Stock> findStockByManufacturer(String manufacturer) {
        return stockDAO.findByManufacturer(manufacturer);
    }
}
