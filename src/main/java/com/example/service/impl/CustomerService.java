package com.example.service.impl;

import com.example.dao.CustomerDAO;
import com.example.entity.Customer;
import com.example.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerService implements ICustomerService{

    private CustomerDAO customerDAO;

    @Autowired
    public void setCustomerDAO(CustomerDAO c){this.customerDAO = c;}


    @Override
    public List<Customer> findAll() {
        return customerDAO.findAll();
    }

    @Override
    public void addACustomer(Customer customer) {
        customerDAO.save(customer);
    }

    @Override
    public Customer findCustomerById(int id) {
        return customerDAO.findById(id);
    }

    @Override
    public Customer findCustomerByEmail(String email) {
        return customerDAO.findByEmail(email);
    }
}
