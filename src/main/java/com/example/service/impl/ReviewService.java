package com.example.service.impl;

import com.example.dao.ReviewDAO;
import com.example.entity.Customer;
import com.example.entity.Review;
import com.example.entity.Stock;
import com.example.service.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewService implements IReviewService{

    private ReviewDAO reviewDAO;

    @Autowired
    public void setReviewDAO(ReviewDAO r){this.reviewDAO = r;}


    @Override
    public List<Review> findAll() {
        return reviewDAO.findAll();
    }

    @Override
    public void addAReview(Review review) {
        reviewDAO.save(review);
    }

}
