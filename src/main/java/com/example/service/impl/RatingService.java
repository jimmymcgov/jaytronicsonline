package com.example.service.impl;


import com.example.dao.RatingDAO;
import com.example.entity.Customer;
import com.example.entity.Rating;
import com.example.entity.Stock;
import com.example.service.IRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RatingService implements IRatingService{

    private RatingDAO ratingDAO;

    @Autowired
    public void setRatingDAO(RatingDAO r){this.ratingDAO = r;}

    @Override
    public List<Rating> findAll() {
        return null;
    }

    @Override
    public void addARating(Rating rating) {
        ratingDAO.save(rating);
    }

//    @Override
//    public List<Rating> findAll() {
//        return ratingDAO.findAll();
//    }
//
//    @Override
//    public void addARating(Rating rating) {
//        ratingDAO.save(rating);
//    }
//
//    @Override
//    public List<Rating> findRatingByStock(Stock stock) {
//        return ratingDAO.findByStock(stock);
//    }
//
//    @Override
//    public List<Rating> findRatingByCustomer(Customer customer) {
//        return ratingDAO.findByCustomer(customer);
//    }
}
