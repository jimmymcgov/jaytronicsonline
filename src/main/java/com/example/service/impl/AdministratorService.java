package com.example.service.impl;

import com.example.dao.AdministratorDAO;
import com.example.entity.Administrator;
import com.example.service.IAdministratorService;
import java.util.concurrent.atomic.AtomicReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AdministratorService implements IAdministratorService {

    private AdministratorDAO administratorDOA;

    private static AtomicReference<AdministratorService> INSTANCE = new AtomicReference<AdministratorService>();

    public AdministratorService() {
        final AdministratorService previous = INSTANCE.getAndSet(this);
        if (previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static AdministratorService getInstance() {
        return INSTANCE.get();
    }

    @Autowired
    public void setAdministratorDOA(AdministratorDAO a){this.administratorDOA = a;}

    @Override
    public List<Administrator> findAll() {
        return administratorDOA.findAll();
    }


    @Override
    public void addAnAdministrator(Administrator administrator) {
        administratorDOA.save(administrator);
    }

    @Override
    public Administrator findAdminByEmail(String email) {
        return administratorDOA.findByEmail(email);
    }


}
