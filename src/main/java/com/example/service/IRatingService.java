package com.example.service;

import com.example.entity.Customer;
import com.example.entity.Rating;
import com.example.entity.Stock;

import java.util.List;

public interface IRatingService {

    List<Rating> findAll();
    void addARating(Rating rating);
//    List<Rating> findRatingByStock(Stock stock);
//    List<Rating> findRatingByCustomer(Customer customer);
//}
}
