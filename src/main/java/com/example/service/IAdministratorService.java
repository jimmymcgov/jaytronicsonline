package com.example.service;

import com.example.entity.Administrator;
import java.util.List;

public interface IAdministratorService {

    List<Administrator> findAll();
    void addAnAdministrator(Administrator administrator);
    Administrator findAdminByEmail(String email);

}
